$(document).ready(function () {

    $('.header-carousel.owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayHoverPause: true,
        dots: true,
        lazyLoad: true,
        nav: true,
        items: 1,
        navText: [
            '<i class="icon-arrow-left"></i>',
            '<i class="icon-arrow-right"></i>'
        ]
    });

    $(".products-carousel.owl-carousel").each(function(e){
        $(this).owlCarousel({
            loop: true,
            responsiveClass: true,
            dots: false,
            nav: true,
            lazyLoad: true,
            navContainer: `.pcn${e < 1 ? 0 : e}`,
            navText: [
                '<i class="icon-arrow-left"></i>',
                '<i class="icon-arrow-right"></i>'
            ],
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        });
    });

    const navbar = $('#navbar-main'),
        distance = navbar.offset().top,
        $window = $(window);
    const headerButtons = $('.main__header--buttons'),
        hDistance = headerButtons.offset().left;
    $window.scroll(function() {
        if ($window.scrollTop() >= distance) {
            navbar.addClass('is-sticky');
            $('main').addClass('is-sticky');
            $(headerButtons).addClass('is-sticky');
            headerButtons.css('left', `${hDistance}px`);
        } else {
            navbar.removeClass('is-sticky');
            $('main').removeClass('is-sticky');
            $(headerButtons).removeClass('is-sticky');
            headerButtons.css('left', '');
        }
    });

    $(document).on('click.collapse', '[data-toggle=icon-menu]', function(e) {
        let $this = $(this),
            target = $this.attr('data-target'),
            navbar = $('.navbar-nav'),
            overlay = $('.overlay');
        navbar.toggleClass('is-active');
        navbar.not(target).removeClass('is-active');
        if (navbar.hasClass('is-active')) {
            overlay.addClass('is-active');
        } else {
            overlay.removeClass('is-active');
        }
    });

    $('.overlay').click(function () {
        $(this).removeClass('is-active');
        $('.navbar-nav').removeClass('is-active');
    });

    $('.dropdown-sublist-back').click(function () {
        $(this).parent().parent().removeClass('show');
    });

    $(document).on('mouseenter.collapse', '[data-toggle=collapse]', function(e) {
        let $this = $(this),
            target = $this.attr('data-target'),
            option = $(target).hasClass('in') ? 'hide' : 'show';
        $('.dropdown-sublist').not(target).collapse('hide');
        $(target).collapse(option);
        $this.children("a").addClass('active');
    }).on('mouseleave.collapse', '[data-toggle=collapse]', function() {
        let $this = $(this),
            target = $this.attr('data-target'),
            option = $(target).hasClass('in') ? 'show' : 'hide',
            selected = `${target}.dropdown-sublist`;
        if ($(selected).is(':hover')) {
            $(target).collapse('show');
            $(selected).on('mouseleave', function () {
                if ($this.is(':hover')) {
                    $(target).collapse('show');
                } else {
                    $this.children("a").removeClass('active');
                    $(target).collapse('hide');
                }
            })
        } else {
            $this.children("a").removeClass('active');
            $(target).collapse(option);
        }
    });

    $('.btn-filter-toggle').click(function (e) {
        e.preventDefault();
        $('.main__list--filter').toggleClass('is-active');
        $('body').toggleClass('filter');
    });

    $('.filter-close-btn').click(function (e) {
        e.preventDefault();
        $('.main__list--filter').toggleClass('is-active');
        $('body').toggleClass('filter');
    });
});
