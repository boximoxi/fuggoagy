# Függőágy.hu

### Includes

- Bootstrap 5 (SCSS, JS)
- jQuery
- Popper
- Owl Carousel

### Installation

```
npm install
```

- '`!noweb_html/index.html`' as home page
- JavaScripts: `js/scripts.js`
- SCSS: `src/scss/app.scss`
```
npm run build
``` 

### Tasks
- Build sources - ```npm run build```
- Start webpack dev server - ```npm run start```
- Build sources for production (**with optimization**) - ```npm run production```
- Clean '`dist`' folder - ```npm run clear```
